import Ball from '../assets/icons/horizontalHeadingIcons/ball.svg';
import BallActive from '../assets/icons/horizontalHeadingIcons/ball-active';
import Maps from '../assets/icons/horizontalHeadingIcons/maps.svg';
import MapsActive from '../assets/icons/horizontalHeadingIcons/maps-active.svg';
import Monitor from '../assets/icons/horizontalHeadingIcons/monitor.svg';
import MonitorActive from '../assets/icons/horizontalHeadingIcons/monitor-active.svg';
import Tag from '../assets/icons/horizontalHeadingIcons/tag.svg';
import TagActive from '../assets/icons/horizontalHeadingIcons/tag-active.svg';
import Wallet from '../assets/icons/horizontalHeadingIcons/wallet.svg';
import WalletActive from '../assets/icons/horizontalHeadingIcons/wallet-active.svg';
import AutoBussiness from '../assets/icons/horizontalHeadingIcons/Автобизнес.svg'
import Administration from '../assets/icons/horizontalHeadingIcons/Администрация.svg'
import ServiceArea from '../assets/icons/horizontalHeadingIcons/service-area.svg'
import Retail from '../assets/icons/horizontalHeadingIcons/retail.svg'
import Accounting from '../assets/icons/horizontalHeadingIcons/accounting.svg'
import Finance from '../assets/icons/horizontalHeadingIcons/finances.svg'
import Bank from '../assets/icons/horizontalHeadingIcons/bank.svg'
import IT from '../assets/icons/horizontalHeadingIcons/IT.svg'
import Tourism from '../assets/icons/horizontalHeadingIcons/tourism.svg'
import HotelBussiness from '../assets/icons/horizontalHeadingIcons/hotel.svg'
import Logistic from '../assets/icons/horizontalHeadingIcons/logistics.svg'
import Secretariat from '../assets/icons/horizontalHeadingIcons/secretariat.svg'
import TV from '../assets/icons/horizontalHeadingIcons/TV.svg'
import Building from '../assets/icons/horizontalHeadingIcons/build.svg'
import Agriculture from '../assets/icons/horizontalHeadingIcons/agriculture.svg'
import Marketing from '../assets/icons/horizontalHeadingIcons/marketing.svg'
import PR from '../assets/icons/horizontalHeadingIcons/PR.svg'
import Education from '../assets/icons/horizontalHeadingIcons/education.svg'
import Medicine from '../assets/icons/horizontalHeadingIcons/medicine.svg'
import Farmacy from '../assets/icons/horizontalHeadingIcons/farmacy.svg'
import TopManagment from '../assets/icons/horizontalHeadingIcons/top-managment.svg'
import Design from '../assets/icons/horizontalHeadingIcons/design.svg'
import HR from '../assets/icons/horizontalHeadingIcons/HR.svg'
import Media from '../assets/icons/horizontalHeadingIcons/media.svg'
import Beauty from '../assets/icons/horizontalHeadingIcons/beauty.svg'
import Sport from '../assets/icons/horizontalHeadingIcons/sport.svg'
import Jurisprudence from '../assets/icons/horizontalHeadingIcons/jurisprudence.svg'
import Property from '../assets/icons/horizontalHeadingIcons/property.svg'
import Security from '../assets/icons/horizontalHeadingIcons/security.svg'
import Culture from '../assets/icons/horizontalHeadingIcons/culture.svg'
import Insurance from '../assets/icons/horizontalHeadingIcons/insurance.svg'

const horizontalHeadingIcons = {
  Ball: Ball,
  BallActive: BallActive,
  Maps: Maps,
  MapsActive: MapsActive,
  Monitor: Monitor,
  MonitorActive: MonitorActive,
  Tag: Tag,
  TagActive: TagActive,
  Wallet: Wallet,
  WalletActive: WalletActive,
  AutoBussiness: AutoBussiness,
  Administration: Administration,
  ServiceArea: ServiceArea,
  Retail: Retail,
  Accounting: Accounting,
  Finance: Finance,
  Bank: Bank,
  IT: IT,
  Tourism: Tourism,
  HotelBussiness: HotelBussiness,
  Logistic: Logistic,
  Secretariat: Secretariat,
  TV: TV,
  Building: Building,
  Agriculture: Agriculture,
  Marketing: Marketing,
  PR: PR,
  Education: Education,
  Medicine: Medicine,
  Farmacy: Farmacy,
  TopManagment: TopManagment,
  Design: Design, 
  HR: HR,
  Media: Media,
  Beauty: Beauty,
  Sport: Sport,
  Jurisprudence: Jurisprudence,
  Property: Property,
  Security: Security,
  Culture: Culture,
  Insurance: Insurance,
};

export default horizontalHeadingIcons;
