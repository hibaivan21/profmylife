import React, { Component } from 'react';
import { View, StatusBar, Text, FlatList } from 'react-native';

import DefaultHeader from '../../../../components/Headers/DefaultHeader';
import { globalStyles, colors } from '../../../../constants';
import DefaultInput from '../../../../components/inputs/DefaultInput';
import LoadingStatus from '../../../../components/LoadingStatus'
import styles from './styles';
import { Icon } from 'react-native-elements';
import staticData from '../../../../constants/staticData';
import { TouchableOpacity } from 'react-native-gesture-handler';

class FilterCity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: null,
      loading: false,
    };
  }

  componentDidMount() {
    this.getCities()
  }

  getCities = () => {

    const requestOptions = {
      method: 'GET',
      headers: new Headers({
        ['Authorization']: `JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTk0MDIxNDkzLCJqdGkiOiIyNmI2MjZmNjA2YjY0OWE4YmJiMDdmY2NkYTU4ZWRlZSIsInVzZXJfaWQiOjF9.xNmijCr0_9giKbeGjLr_vmf9pY-udtctA0Dpn-fO_r0`,
        'Content-Type': 'application/json'
      })
    };

    fetch('http://proffmylife.dev.appsider.net:8000/api/catalogs/city/', requestOptions)
      .then(res => res.json())
      .then(response => this.setState({cities: response}))
  }

  setCity = (item) => {
    const { setFilterCities, navigation } = this.props;

    setFilterCities(item)
    navigation.goBack()
  }


  render() {
    const { navigation, setFilterCities } = this.props;
    const { loading, cities } = this.state
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="transparent" translucent />
        <View style={{ flexGrow: 1 }}>
          <DefaultHeader
            headerCenter="Город"
            headerLeft={
              <Icon
                underlayColor="transparent"
                name="chevron-thin-left"
                type="entypo"
                color="white"
                onPress={() => navigation.goBack()}
              />
            }
            headerRight={
              <Icon
                underlayColor="transparent"
                name="check"
                type="material"
                color="white"
                size={30}
                onPress={() => navigation.goBack()}
              />
            }
          />
          <View style={globalStyles.container}>
            <View style={styles.inputContainer}>
              <DefaultInput
                rightIcon={
                  <Icon
                    name="search"
                    type="octicon"
                    color={colors.BUTTON_GRADIENT_END}
                  />
                }
              />
            </View>
            {cities === null ? <LoadingStatus /> : null} 
            <View style={styles.flatListContainer}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.state.cities === null ? staticData.cities : this.state.cities}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.setCity(item)}
                  >
                    <Text style={[globalStyles.SFR_16_Gray, { marginVertical: 5 }]}>
                      {item.description_ru}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default FilterCity;
