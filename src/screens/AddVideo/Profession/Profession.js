import React, { Component } from 'react';
import { View, StatusBar, Text, FlatList } from 'react-native';
import LoadingStatus from '../../../components/LoadingStatus'

import DefaultHeader from '../../../components/Headers/DefaultHeader';
import { globalStyles, colors } from '../../../constants';
import DefaultInput from '../../../components/inputs/DefaultInput';

import styles from './styles';
import { Icon } from 'react-native-elements';
import staticData from '../../../constants/staticData';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Profession extends Component {
  constructor(props) {
    super(props);
    this.state = {
      professions: null,
      loading: false
    };
  }

  componentDidMount() {
    this.getProfessions()
  }

  getProfessions = () => {
    const requestOptions = {
      method: 'GET',
      headers: new Headers({
        ['Authorization']: `JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTk0MDIxNDkzLCJqdGkiOiIyNmI2MjZmNjA2YjY0OWE4YmJiMDdmY2NkYTU4ZWRlZSIsInVzZXJfaWQiOjF9.xNmijCr0_9giKbeGjLr_vmf9pY-udtctA0Dpn-fO_r0`,
        'Content-Type': 'application/json'
      })
    };

    fetch('http://proffmylife.dev.appsider.net:8000/api/catalogs/profession/', requestOptions)
      .then(res => res.json())
      .then(response => this.setState({professions: response}))
  }


  onPressItem = (item) => {
    const { navigation, setProfession } = this.props;
    setProfession(item)
    navigation.navigate('YourVideo')
  }
  render() {
    const { navigation } = this.props;
    const { loading, professions } = this.state

    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="transparent" translucent />
        <View style={{ flex: 1 }}>
          <DefaultHeader
            headerCenter="Профессия"
            headerLeft={
              <Icon
                underlayColor="transparent"
                name="chevron-thin-left"
                type="entypo"
                color="white"
                onPress={() => navigation.goBack()}
              />
            }
            headerRight={
              <Icon
                underlayColor="transparent"
                name="check"
                type="material"
                color="white"
                size={30}
                onPress={() => navigation.navigate('YourVideo')}
              />
            }
          />
          <View style={globalStyles.container}>
            <View style={styles.inputContainer}>
              <DefaultInput
                rightIcon={
                  <Icon
                    name="search"
                    type="octicon"
                    color={colors.BUTTON_GRADIENT_END}
                  />
                }
              />
            </View>
            {professions === null ? <LoadingStatus /> : null} 
            <View style={styles.flatListContainer}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={professions === null ? staticData.professionsList : professions}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    //onPress={this.onPressItem(1)}
                    onPress={() => navigation.navigate('YourVideo')}
                  >
                    <Text style={[globalStyles.SFR_16_Gray, { marginVertical: 5 }]}>
                      {item}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default Profession;
