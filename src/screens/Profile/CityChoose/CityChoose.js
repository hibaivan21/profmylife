import React, { Component, PureComponent } from 'react';
import { View, StatusBar, Text, SafeAreaView, FlatList, Button } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LoadingStatus from '../../../components/LoadingStatus'

import DefaultHeader from '../../../components/Headers/DefaultHeader';
import { globalStyles, icons, images, colors } from '../../../constants';
import DefaultInput from '../../../components/inputs/DefaultInput';

import styles from './styles';
import { Icon } from 'react-native-elements';
import staticData from '../../../constants/staticData';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage'

class CityChoose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: null,
      loading: false
    };
  }

  getCities = () => {

    const requestOptions = {
      method: 'GET',
      // headers: new Headers({
      //   ['Authorization']: `JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTk0MDIxNDkzLCJqdGkiOiIyNmI2MjZmNjA2YjY0OWE4YmJiMDdmY2NkYTU4ZWRlZSIsInVzZXJfaWQiOjF9.xNmijCr0_9giKbeGjLr_vmf9pY-udtctA0Dpn-fO_r0`,
      //   'Content-Type': 'application/json'
      // })
    };

    fetch('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0', requestOptions)
      .then(res => res.json())
      .then(response => console.log(response))
  }

  componentDidMount() {
    this.getCities()
  }

  onSubmit = async city => {
    const { navigation } = this.props
    try {
      await AsyncStorage.setItem('city', city)
    } catch (error) {
      console.log(error)
    }
    navigation.goBack()
  }

  updateCity = city => {
    const { navigation, setCity } = this.props
    setCity(city)
    navigation.goBack()
  }

  render() {
    const { navigation } = this.props;
    const { loading, cities } = this.state
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="transparent" translucent />
        <View style={{ flexGrow: 1 }}>
          <DefaultHeader
            headerCenter="Город"
            headerLeft={
              <Icon
                underlayColor="transparent"
                name="chevron-thin-left"
                type="entypo"
                color="white"
                onPress={() => navigation.goBack()}
              />
            }
            headerRight={
              <Icon
                underlayColor="transparent"
                name="check"
                type="material"
                color="white"
                size={30}
                onPress={() => navigation.goBack()}
              />
            }
          />
          <View style={globalStyles.container}>
            <View style={styles.inputContainer}>
              <DefaultInput
                rightIcon={
                  <Icon
                    name="search"
                    type="octicon"
                    color={colors.BUTTON_GRADIENT_END}
                  />
                }
              />
            </View>
            {cities === null ? <LoadingStatus /> : null} 
            <View style={styles.flatListContainer}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.state.cities === null ? staticData.cities : this.state.cities}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => this.onSubmit(item)}>
                    <Text style={[globalStyles.SFR_16_Gray, { marginVertical: 5 }]}>
                      {item.description_ru}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}



export default CityChoose;
